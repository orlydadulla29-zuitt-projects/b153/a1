const User = require("../models/user");

module.exports.checkEmail = (body) => {
	//The mongoDB find() method ALWAYS returns an array
	return User.find({email: body.email}).then(result => {
		if(result.length > 0){
			return true; //true means "yes, email exists"
		}else{
			return false; //false means "no, email does not exist"
		}
	})
}


/*

Activity:

Create a function named register that processed the user's submitted data and creates a new user record in our database. The process flow for this is very similar to creating a new course, with the main differences being the model to use and the data needed to create.

Check your work in Postman by creating a new route called Register New User" and also check MongoDB Atlas if the user was created in the database.

*/


module.exports.register = (body) => {


	let newUser = new User({
		
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		password: body.password,
		mobileNo: body.mobileNo

	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})

}